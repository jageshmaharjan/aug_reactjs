import React, { useState, useEffect } from "react";
import logo from "./logo.svg";
import "./App.css";
import axios from "axios";
import Loader from "react-loader-spinner";

function App() {
  const [response, setReponse] = useState("ReactJs Demo");
  const [isLoading, setLoading] = useState(true);

  const config = {
    headers: { "Access-Control-Allow-Origin": "*" },
  };

  useEffect(() => {
    axios.get("http://165.22.223.35:6464/", config).then((response) => {
      setLoading(false);
      setReponse(response.data.payload);
    });
  });

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />

        {isLoading ? (
          <Loader type="ThreeDots" color="#2BAD60" height="100" width="100" />
        ) : (
          <a
            className="App-link"
            href="https://ace.atlassian.com/kathmandu/"
            target="_blank"
            rel="noopener noreferrer"
          >
            Hooray! {response}
          </a>
        )}
      </header>
    </div>
  );
}

export default App;
