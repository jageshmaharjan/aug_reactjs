import React from "react";
import { render } from "@testing-library/react";
import App from "./App";

test("renders learn react link", () => {
  const { getByText } = render(<App />);
  const linkElement = getByText("AUG Meetup 2020");
  expect(linkElement).toBeInTheDocument();
});
